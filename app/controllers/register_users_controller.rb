class RegisterUsersController < ApplicationController
  def new
    @register_user = RegisterUser.build
  end

  def create
    @register_user = RegisterUser.new(register_user_params)

    unless @register_user.valid?
      render :new, status: :unprocessable_entity
      return
    end

    # Don't forget to validate the input

    Identity::Client::Register.(
      identity_id: @register_user.id,
      email: @register_user.email,
      password_hash: @register_user.password_hash
    )

    redirect_to "/register/registration-complete" 
  end

  def show
  end

  def register_user_params
    params.require(:register_user).permit(:id, :email, :password)
  end
end
