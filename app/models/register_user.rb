require "bcrypt"
require "identifier/uuid"

class RegisterUser
  include ActiveModel::API

  attr_accessor :id, :email, :password
  attr_writer :password_hash

  #validates :id#, presence: true
  validates :email,
            format: {
              with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i,
              message: "is not an email address"
            }
  validates :password,
            length: { minimum: 8 }#, allow_blank: false

  def self.build
    instance = new

    instance.id = Identifier::UUID::Random.get

    instance
  end

  def password_hash
    @password_hash ||= BCrypt::Password.create(password)
  end
end
