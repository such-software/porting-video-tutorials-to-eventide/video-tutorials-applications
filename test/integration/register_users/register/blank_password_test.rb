require "test_helper"
require "video_tutorials_applications/register_users/controls"

require "messaging"

module RegisterUsersTests
  class BlankPassword < ActionDispatch::IntegrationTest
    test "Register command" do
      identity_id = VideoTutorialsApplications::RegisterUsers::Controls::Id.example
      email = VideoTutorialsApplications::RegisterUsers::Controls::Email.example
      password = ""

      post register_path,
        params: {
          register_user: {
            id: identity_id,
            email:,
            password: 
          }
        }

      assert_response :unprocessable_entity
      assert_select ".invalid-feedback", "Password is too short (minimum is 8 characters)"

      session = MessageStore::Postgres::Session.build

      stream_name = "identity:command-#{identity_id}"

      count_result = session.execute("SELECT COUNT(*) AS c FROM messages WHERE stream_name=$1", [stream_name])
      messages_written = count_result.first["c"]

      assert messages_written == 0
    end
  end
end


