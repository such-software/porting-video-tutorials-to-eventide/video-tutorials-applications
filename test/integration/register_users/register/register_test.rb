require "test_helper"
require "video_tutorials_applications/register_users/controls"

require "messaging"

module RegisterUsersTests
  class RegisterTest < ActionDispatch::IntegrationTest
    test "Register command" do
      identity_id = VideoTutorialsApplications::RegisterUsers::Controls::Id.example
      email = VideoTutorialsApplications::RegisterUsers::Controls::Email.example
      password = VideoTutorialsApplications::RegisterUsers::Controls::Password.example

      post register_path,
        params: {
          register_user: {
            id: identity_id,
            email:,
            password: 
          }
        }

      assert_response :redirect
      follow_redirect!
      assert_response :success
      assert_select "p", "You have successfully registered!"

      session = MessageStore::Postgres::Session.build

      stream_name = "identity:command-#{identity_id}"

      count_result = session.execute("SELECT COUNT(*) AS c FROM messages WHERE stream_name=$1", [stream_name])
      messages_written = count_result.first["c"]

      assert messages_written == 1

      message_data = MessageStore::Postgres::Get::Stream::Last.(stream_name, session:)
      register = Messaging::Message::Import.(message_data, Identity::Client::Messages::Commands::Register)

      assert register.identity_id == identity_id
      assert register.email == email

      bcrypt = BCrypt::Password.new(register.password_hash)
      assert bcrypt == password
    end
  end
end
