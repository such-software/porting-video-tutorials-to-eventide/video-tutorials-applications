require "test_helper"
require "video_tutorials_applications/register_users/controls"

require "messaging"

module RegisterUsersTests
  class BlankEmail < ActionDispatch::IntegrationTest
    test "Register command" do
      identity_id = VideoTutorialsApplications::RegisterUsers::Controls::Id.example
      email = ""
      password = VideoTutorialsApplications::RegisterUsers::Controls::Password.example

      post register_path,
        params: {
          register_user: {
            id: identity_id,
            email:,
            password: 
          }
        }

      assert_response :unprocessable_entity
      assert_select ".invalid-feedback", "Email is not an email address" 

      session = MessageStore::Postgres::Session.build

      stream_name = "identity:command-#{identity_id}"

      count_result = session.execute("SELECT COUNT(*) AS c FROM messages WHERE stream_name=$1", [stream_name])
      messages_written = count_result.first["c"]

      assert messages_written == 0
    end
  end
end


