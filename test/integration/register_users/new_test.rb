require "test_helper"

module RegisterUsersTests
  UUID_REGEX = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/

  class NewTest < ActionDispatch::IntegrationTest
    test "It gets the form path" do
      get register_path

      assert_response :success
      assert_select "input[type=hidden]", value: UUID_REGEX
    end
  end
end
