module VideoTutorialsApplications
  module RegisterUsers
    module Controls
      Email = Identity::Client::Controls::Email

      module Email
        module NotAnEmail
          def self.example
            "notanemail"
          end
        end
      end
    end
  end
end
