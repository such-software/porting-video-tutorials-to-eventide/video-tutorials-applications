module VideoTutorialsApplications
  module RegisterUsers
    module Controls
      module Id
        def self.example
          Identifier::UUID::Random.get
        end
      end
    end
  end
end
