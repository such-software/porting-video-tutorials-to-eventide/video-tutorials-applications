Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # We're not creating a registration.  We're registering.
  get "/register", to: "register_users#new"
  get "/register/registration-complete", to: "register_users#show"
  resource :register, controller: 'register_users', only: [:create]

  # Defines the root path route ("/")
  root "home#index"
end
